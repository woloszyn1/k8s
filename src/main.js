import { createApp } from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'

const app = createApp(App).use(router)
app.use(vuetify)
app.use(VueRouter)

app.mount('#app')
